import Adapt from "@adpt/core";

import { NodeService } from "@adpt/cloud/nodejs";
import { k8sTestStyle, laptopStyle } from "./styles";

function App() {
    return <NodeService srcDir="../backend" scope="external" />;
}

Adapt.stack("default", <App />, laptopStyle);
Adapt.stack("laptop", <App />, laptopStyle);
Adapt.stack("k8s-test", <App />, k8sTestStyle());
